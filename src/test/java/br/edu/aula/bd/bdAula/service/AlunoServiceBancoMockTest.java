package br.edu.aula.bd.bdAula.service;

import br.edu.aula.bd.bdAula.dao.AlunoDAO;
import br.edu.aula.bd.bdAula.model.Aluno;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles({"test"})
public class AlunoServiceBancoMockTest {

    @MockBean
    AlunoDAO a;
    @Autowired
    AlunoService aServ;

    @Test
    @DisplayName("teste aluno aprovado")
    public void teste001(){
        int idAluno = 1;
        //a.findById(id).get() zezin 10
        Aluno aRet = new Aluno(1,"zezin",10);
        Mockito.when(a.getReferenceById(1)).thenReturn(aRet);
        String esperado = "Aluno não está de final";

        try {
            double resultado =  aServ.precisaTirarNaFinal(idAluno);
            Assertions.fail("erro!!!!!!");
        } catch (Exception e) {
            Assertions.assertEquals(esperado, e.getMessage());
        }
    }
    @Test
    @DisplayName("teste aluno reprovado")
    public void teste002(){
        int idAluno = 1;
        //a.findById(id).get() gustin 10
        Aluno aRet = new Aluno(1,"gustin",1);
        Mockito.when(a.getReferenceById(1)).thenReturn(aRet);
        String esperado = "Aluno não está de final";

        try {
            double resultado =  aServ.precisaTirarNaFinal(idAluno);
            Assertions.fail("erro!!!!!!");
        } catch (Exception e) {
            Assertions.assertEquals(esperado, e.getMessage());
        }
    }
    @Test
    @DisplayName("teste aluno de final com nota 5")
    public void teste003(){
        int idAluno = 2;
        //a.findById(id).get() gustin 1
        Aluno aRet = new Aluno(1,"gustin",5);
        Mockito.when(a.getReferenceById(1)).thenReturn(aRet);
        double esperado = 9;

        try {
            double resultado =  aServ.precisaTirarNaFinal(idAluno);
            Assertions.fail("erro!!!!!!");
        } catch (Exception e) {
            Assertions.assertEquals(esperado, e.getMessage());
        }
    }
    @Test
    @DisplayName("teste aluno não existe")
    public void teste004(){
        int idAluno = 1;
        //a.findById(id).get() gustin 1
        Aluno aRet = new Aluno(1,"gustin",1);
        Mockito.when(a.getReferenceById(1)).thenReturn(null);

        String esperado = "Aluno não encontrado";
        try {
            double resultado =  aServ.precisaTirarNaFinal(idAluno);
            Assertions.fail("erro!!!!!!");
        } catch (Exception e) {
            Assertions.assertEquals(esperado, e.getMessage());
        }
    }
    @Test
    @DisplayName("teste aluno não existe")
    public void teste005(){
        int idAluno = 1;
        //a.findById(id).get() gustin 1
        Aluno aRet = new Aluno(1,"gustin",1);
        Mockito.when(a.getReferenceById(1)).thenReturn(null);

        String esperado = "Aluno não encontrado";
        try {
            double resultado =  aServ.precisaTirarNaFinal(idAluno);
            Assertions.fail("erro!!!!!!");
        } catch (Exception e) {
            Assertions.assertEquals(esperado, e.getMessage());
        }
    }
}

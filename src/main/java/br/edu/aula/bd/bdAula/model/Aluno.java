package br.edu.aula.bd.bdAula.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Aluno {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String nome;
    private double nota;

    public boolean isAprovade(){
        return nota >= 7;
    }
    public boolean isReprove(){
        return nota < 4;
    }
    public boolean isFinal(){
        return !isAprovade() && !isReprove();
    }
}

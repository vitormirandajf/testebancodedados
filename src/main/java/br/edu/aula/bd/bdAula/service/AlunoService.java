package br.edu.aula.bd.bdAula.service;

import br.edu.aula.bd.bdAula.dao.AlunoDAO;
import br.edu.aula.bd.bdAula.model.Aluno;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlunoService {
    @Autowired
    AlunoDAO aDao;

    public Aluno salvarAluno(Aluno a) throws Exception {
        if (a.getNome() == null || a.getNome().isEmpty()){
            throw new Exception("nome incorreto");
        }
        if (a.getNota() < 0 || a.getNota() > 10){
            throw new Exception("nota incorreta");
        }
        return aDao.save(a);
    }

    public List<Aluno> alunosAprovados(){
        List<Aluno> lista = aDao.findByNotaGreaterThan(6.9999999999);
        return lista;
    }

    public double precisaTirarNaFinal(int id) throws Exception {
    Aluno a = aDao.getReferenceById(id);
        if(a == null){
            throw new Exception("Aluno não encontrado");
        }
        if(!a.isFinal()){
            throw new Exception("Aluno não está de final");
        }
        return 14 - a.getNota();
    }
}

